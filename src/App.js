import React, { useEffect, useState } from "react";
import useLocation from "./hooks/useLocation";
import useWindow from "./hooks/useWindow";
import useClipboard from "./hooks/useClipboard";
import useArrayOperation from "./hooks/useArrayOperation";
import useValidation from "./hooks/useValidation";
import useData from "./hooks/useData";
import useLocalStorage from "./hooks/useLocalStorage";
import useSessionStorage from "./hooks/useSessionStorage";

import loading from "./loading.gif";

import "./App.css";

function App() {
    // useLocation hook for location
    const { latitude, longitude } = useLocation();

    // useWindow hook for screen size
    const [screen, setScreen] = useState(false);
    const { height, width, screenType } = useWindow();

    // useArrayOperation hook for array operations
    const [choice, setChoice] = useState("Select");
    const { array } = useArrayOperation(choice);

    // useClipboard hook for copy to clipboard
    const [text, setText] = useState("");
    useClipboard(text);

    // useValidation hook for state validation
    const [name, setName] = useState("");
    const { msg } = useValidation(name);

    // useData hook for GETting data
    const [getData, setGetData] = useState(false);
    const { data, load } = useData({
        url: "https://fakestoreapi.com/products/",
        option: "GET",
        dependencies: getData,
    });

    // useLocalStorage and useSessionStorage for local and session storage
    const [localData, setLocalData] = useState(false);
    const [sessionData, setSessionData] = useState(false);
    const { local, setLocal } = useLocalStorage();
    const { session, setSession } = useSessionStorage();

    const copyText = () => {
        setText("You have copied this text");
    };

    const arrayOperation = (e) => {
        setChoice(e.target.value);
    };

    useEffect(() => {
        setLocalData(localStorage.getItem("name"));
    }, [local]);
    useEffect(() => {
        setSessionData(sessionStorage.getItem("name"));
    }, [session]);

    return (
        <div className="hook-wrapper">
            <div className="location">
                {/* useLocation hook */}
                <p>Latitude: {latitude}</p>
                <p>Longitude: {longitude}</p>
            </div>
            <div className="window">
                {/* useWindow hook */}
                <button
                    className="btn btn-dark"
                    onClick={() => setScreen(!screen)}
                >
                    {!screen ? "Get Height and Width" : "Hide Height and Width"}
                </button>
                {screen && (
                    <div>
                        <span>Height: {height}</span>
                        <span>Width: {width}</span>
                        <p>{screenType}</p>
                    </div>
                )}
            </div>
            <div className="copy">
                {/* useClipboard hook */}
                <button className="btn btn-dark" onClick={copyText}>
                    Copy text
                </button>
                <p>You can copy this text</p>
            </div>
            <div className="array-ops">
                {/* useArrayOperation hook */}
                <select onChange={arrayOperation}>
                    <option value="Select">Select</option>
                    <option value="Push">Push</option>
                    <option value="Pop">Pop</option>
                    <option value="Shift">Shift</option>
                </select>
                <p>
                    {array.map((ele, i) => {
                        return <span key={i}>{ele}</span>;
                    })}
                </p>
            </div>
            <div className="validation">
                {/* useValidation hook */}
                <form>
                    <input
                        type="text"
                        placeholder="Enter your name"
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                    />
                    <p>{msg}</p>
                </form>
            </div>
            <div className="get-data">
                {/* useData hook */}
                <button
                    className="btn btn-dark get-data-btn"
                    onClick={() => setGetData(!getData)}
                >
                    {getData ? "Hide Data" : "Get Data"}
                </button>
                <div>
                    {load ? (
                        <img src={loading} alt="Loading gif"></img>
                    ) : (
                        getData &&
                        data &&
                        data.map((ele, i) => (
                            <img
                                className="data-img"
                                src={ele.image}
                                alt=""
                                key={i}
                            ></img>
                        ))
                    )}
                </div>
            </div>
            <div className="storage">
                {/* useLocalStorage and useSessionStorage hooks */}
                <button
                    className="btn btn-dark local-btn"
                    onClick={() => setLocal({ name: "name", value: "Lakshya" })}
                >
                    Set Local Storage
                </button>
                <div>
                    {localData ? (
                        <p>Data has been stored locally successfully</p>
                    ) : (
                        ""
                    )}
                </div>
                <button
                    className="btn btn-dark session-btn"
                    onClick={() =>
                        setSession({ name: "name", value: "Lakshya" })
                    }
                >
                    Set Session Storage
                </button>
                <div>
                    {sessionData ? (
                        <p>Data has been successfully stored for the session</p>
                    ) : (
                        ""
                    )}
                </div>
            </div>
        </div>
    );
}

export default App;
