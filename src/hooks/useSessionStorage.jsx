import { useEffect, useState } from "react";

const useSessionStorage = () => {
    const [session, setSession] = useState({});

    useEffect(() => {
        const { name, value } = session;
        if (name !== undefined) sessionStorage.setItem(name, value);
    }, [session]);

    return { session, setSession };
};

export default useSessionStorage;
