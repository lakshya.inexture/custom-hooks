import { useEffect, useState } from "react";

const useData = ({ url, option, dependencies }) => {
    const [data, setData] = useState(null);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        const getData = async () => {
            if (dependencies) {
                if (option === "GET") {
                    try {
                        setLoad(true);
                        const response = await fetch(url);
                        const data = await response.json();
                        setData(data);
                        setLoad(false);
                    } catch (error) {
                        alert(error.message);
                    }
                }
            }
        };
        getData();
    }, [dependencies]);
    return { data, load };
};

export default useData;
