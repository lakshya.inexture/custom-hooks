import { useEffect, useState } from "react";

const useArrayOperation = (option) => {
    const [array, setArray] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    useEffect(() => {
        if (option === "Push") setArray([...array, array.length + 1]);
        if (option === "Pop") setArray(array.slice(0, array.length - 1));
        if (option === "Shift")
            setArray(array.slice(1, array[array.length - 1]));
    }, [option]);
    return { array };
};

export default useArrayOperation;
