import { useEffect, useState } from "react";

const useLocalStorage = () => {
    const [local, setLocal] = useState({});

    useEffect(() => {
        const { name, value } = local;

        if (name !== undefined) localStorage.setItem(name, value);
    }, [local]);
    return { local, setLocal };
};

export default useLocalStorage;
