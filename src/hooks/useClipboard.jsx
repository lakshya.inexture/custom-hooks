const useClipboard = (value) => {
    if (value) navigator.clipboard.writeText(value);
};

export default useClipboard;
