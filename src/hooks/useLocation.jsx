import { useState } from "react";

const useLocation = () => {
    const [latitude, setLatitude] = useState("");
    const [longitude, setLongitude] = useState("");

    const location = window.navigator.geolocation;
    if (location) {
        location.getCurrentPosition((pos) => {
            setLatitude(pos.coords.latitude);
            setLongitude(pos.coords.longitude);
        });
    }
    return { latitude, longitude };
};

export default useLocation;
