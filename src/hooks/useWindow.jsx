import { useState, useEffect } from "react";

const useWindow = () => {
    const [height, setHeight] = useState("");
    const [width, setWidth] = useState("");
    const [screenType, setScreenType] = useState("");

    useEffect(() => {
        setHeight(window.innerHeight);
        setWidth(window.innerWidth);
        setScreenType(width < 992 ? "On mobile" : "Not on mobile");
    });

    return { height, width, screenType };
};

export default useWindow;
