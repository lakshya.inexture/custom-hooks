import { useEffect, useState } from "react";

const useValidation = (value) => {
    const [msg, setMsg] = useState("");

    useEffect(() => {
        if (value)
            value.length >= 3
                ? setMsg("")
                : setMsg("Your name cannot be shorter than 3 characters");
    }, [value]);
    return { msg };
};

export default useValidation;
